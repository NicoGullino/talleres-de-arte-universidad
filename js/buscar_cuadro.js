///////////////////////////////JSON//////////////////////////////
var cuadros =[
    {
        "fotos" : [
            {
                "url" : "./imgs/cuadro01_img1.png"
            }
        ],
        "nombre" : "Rosa de Gualupe",
        "descripcion" : "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.",
        "artista" : "Fulanito",
        "anio" : "2020",
        "estilo" : "Realismo",
        "tecnica" : "Pintura al óleo",
        "tipo_publicacion" : "A LA VENTA",
        "cuadro_original" : {
            "caracteristica" : "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
            "tipo_obra" : "Pieza unica",
        }
        ,
        "cuadro_replica" : null

    },
    {
        "fotos" : [
            {
                "url" : "./imgs/cuadro02_img1.png"
            }
        ],
        "nombre" : "Nieve",
        "descripcion" : "Lorem Ipsum is simply dummy text of the printing and typesetting indus",
        "artista" : "Sara Tepes",
        "anio" : "2020",
        "estilo" : "Arte Moderno",
        "tecnica" : "Pintura al óleo",
        "tipo_publicacion" : "A LA VENTA",
        "cuadro_original" : {
            "caracteristica" : "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
            "tipo_obra" : "Pieza seriada",
        },
        "cuadro_replica" : null
    },
    {
        "fotos" : [
            {
                "url" : "./imgs/cuadro03_img1.png"
            },
            {
                "url" : "./imgs/cuadro03_img2.png"
            }
        ],
        "nombre" : "La Mona Lisa",
        "descripcion" : "Lorem Ipsum is simply dummy text of the printing and typesetting indus", 
        "artista" : "Fulanito",
        "anio" : "2020",
        "estilo" : "Renacimiento",
        "tecnica" : "Pintura al óleo",
        "tipo_publicacion" : "VENDIDO",
        "cuadro_original" : null,
        "cuadro_replica" : {
            "autor" : "Leonardo da Vinci",
            "nombre" : "La Mona Lisa",
            "anio" : "1503"

        }

    }
]; 


//var items = [];

/////////////////////////FUNCIONES DE BUSQUEDA DE CUADROS////////////////////////////////


$( document ).ready(function() {
    $("#btn_busqueda").click(function() {
        var items = [];
        div_cuadros = $('#muestra_cuadros').empty();//vaciar
        valor = $('#myInput').val().toLowerCase();

        $(cuadros).filter(function (i,n){
            if(n.artista.toLowerCase()===valor || n.nombre.toLowerCase()===valor){
                items.push(n);
            }
            else if (valor === ''){
                mostrar_cuadros();
            }
        });

        for (var i = 0; i < items.length; i++) { 
            item = items[i];
            fotos = item.fotos;
            id_unico = 'cuadros_' + i;
            div_cuadros.append( "<div id= '" + id_unico +"' class='items'></div>");
            div_cuadro = $('#'+id_unico );
            //mostrar fotoSS. Es una array
            for (var j = 0; j < fotos.length; j++) { 
                foto = fotos[j];
                div_cuadro.append(
                    "<div class= 'cuadros_img'><img src=" + foto.url +" alt='cuadro'></div>");
                }
                // mostrar campos nombre, descripcion, artista,
                //estilo, tecnica, anio, y tipo de publicacion
                elemmnto_nombre = "<p> Nombre: "+item.nombre+ "</p>";
                elemento_descripcion = "<p> Descripcion: "+item.descripcion+ "</p>";
                elemnto_enio = "<p> Año: "+item.anio+ "</p>";
                elemnto_tecnica = "<p> Tecnica: "+item.tecnica+ "</p>";
                elemnto_estilo = "<p> Estilo: "+item.estilo+ "</p>";
                elemnto_tipoPublicacion = "<p> Estado: "+item.tipo_publicacion+ "</p>";
                div_cuadro.append(
                    "<div class= 'texto'>" +elemmnto_nombre + elemnto_enio + elemnto_tecnica + elemnto_estilo+ elemnto_tipoPublicacion +  elemento_descripcion+  "</div>"
                );

            if(item.cuadro_replica == null ){
                // es original. entonces se muestran datos del cuadro original
                original_caracteristica = item.cuadro_original.caracteristica;
                original_tipoObra = item.cuadro_original.tipo_obra;

                elemmnto_original = "<p> Cuadro Original </p>";
                elemmnto_original_caracteristica = "<p> Caracteristica: "+ original_caracteristica + "</p>";
                elemmnto_original_tipoObra = "<p> Tipo de Obra: "+ original_tipoObra + "</p>";
                div_cuadro.append(
                    "<div class= 'texto'>" + elemmnto_original +elemmnto_original_tipoObra + elemmnto_original_caracteristica  +  "</div>"
                );
            }else{
                // es replica. entonces se muestran datos del cuadro replica
                replica_autor = item.cuadro_replica.autor;
                replica_nombre = item.cuadro_replica.nombre;
                replica_anio = item.cuadro_replica.anio;

                elemmnto_replica = "<p> Cuadro Replica </p>";
                elemmnto_replica_autor = "<p> Autor: "+ replica_autor + "</p>";
                elemmnto_replica_nombre = "<p> Nombre: "+ replica_nombre + "</p>";
                elemmnto_replica_anio = "<p> Año: "+ replica_anio + "</p>";

                div_cuadro.append(
                    "<div class= 'texto'>"+ elemmnto_replica +elemmnto_replica_anio + elemmnto_replica_autor + elemmnto_replica_nombre +  "</div>"
                );
            }
            div_cuadro.append("<br>");
        }
    
     });

    function mostrar_cuadros(){
        div_cuadros = $('#muestra_cuadros').empty();
        for (var i = 0; i < cuadros.length; i++) { 
            item = cuadros[i];
            fotos = item.fotos;
            id_unico = 'cuadros_' + i;
            div_cuadros.append( "<div id= '" + id_unico +"' class='items'></div>");
            div_cuadro = $('#'+id_unico );
            //mostrar fotoSS. Es una array
            for (var j = 0; j < fotos.length; j++) { 
                foto = fotos[j];
                div_cuadro.append(
                    "<div class= 'cuadros_img'><img src=" + foto.url +" alt='cuadro'></div>");
                }
                // mostrar campos nombre, descripcion, artista,
                //estilo, tecnica, anio, y tipo de publicacion
                elemmnto_nombre = "<p> Nombre: "+item.nombre+ "</p>";
                elemento_descripcion = "<p> Descripcion: "+item.descripcion+ "</p>";
                elemnto_enio = "<p> Año: "+item.anio+ "</p>";
                elemnto_tecnica = "<p> Tecnica: "+item.tecnica+ "</p>";
                elemnto_estilo = "<p> Estilo: "+item.estilo+ "</p>";
                elemnto_tipoPublicacion = "<p> Estado: "+item.tipo_publicacion+ "</p>";
                div_cuadro.append(
                    "<div class= 'texto'>" +elemmnto_nombre + elemnto_enio + elemnto_tecnica + elemnto_estilo+ elemnto_tipoPublicacion +  elemento_descripcion+  "</div>"
                );

            if(item.cuadro_replica == null ){
                // es original. entonces se muestran datos del cuadro original
                original_caracteristica = item.cuadro_original.caracteristica;
                original_tipoObra = item.cuadro_original.tipo_obra;

                elemmnto_original = "<p> Cuadro Original </p>";
                elemmnto_original_caracteristica = "<p> Caracteristica: "+ original_caracteristica + "</p>";
                elemmnto_original_tipoObra = "<p> Tipo de Obra: "+ original_tipoObra + "</p>";
                div_cuadro.append(
                    "<div class= 'texto'>" + elemmnto_original +elemmnto_original_tipoObra + elemmnto_original_caracteristica  +  "</div>"
                );
            }else{
                // es replica. entonces se muestran datos del cuadro replica
                replica_autor = item.cuadro_replica.autor;
                replica_nombre = item.cuadro_replica.nombre;
                replica_anio = item.cuadro_replica.anio;

                elemmnto_replica = "<p> Cuadro Replica </p>";
                elemmnto_replica_autor = "<p> Autor: "+ replica_autor + "</p>";
                elemmnto_replica_nombre = "<p> Nombre: "+ replica_nombre + "</p>";
                elemmnto_replica_anio = "<p> Año: "+ replica_anio + "</p>";

                div_cuadro.append(
                    "<div class= 'texto'>"+ elemmnto_replica +elemmnto_replica_anio + elemmnto_replica_autor + elemmnto_replica_nombre +  "</div>"
                );
            }
            div_cuadro.append("<br>");
        }        
    }

    mostrar_cuadros();
});