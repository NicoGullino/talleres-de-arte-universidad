////////////////////////////////JSON///////////////////////////////////
var talleres = [
  {
    "nombre_taller" : "Taller de Nicolas",
    "direccion": "Los Polvorines, Buenos Aires",
    "localidad": "Los Polvorines",
    "latitude": -34.5221554,
    "longitude": -58.7000067,
    "descripcion_del_lugar" : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    "descripcion_de_actividades" : "pintura al oleo",
  },
  {
    "nombre_taller" : "Taller de Christian",
    "direccion": "Los Polvorines, Buenos Aires",
    "localidad": "Los Polvorines",
    "latitude": -34.553755, 
    "longitude": -58.692713,
    "descripcion_del_lugar" : "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
    "descripcion_de_actividades" : "acuarela",
  },
  {
    "nombre_taller" : "Taller de Flor",
    "direccion": "Los Polvorines, Buenos Aires",
    "localidad": "Los Polvorines",
    "latitude": -34.516181, 
    "longitude": -58.716625,
    "descripcion_del_lugar" : "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.",
    "descripcion_de_actividades" : "cerámica",
  },
  {
    "nombre_taller" : "Taller de Emanuel",
    "direccion": "Los Polvorines, Buenos Aires", 
    "localidad": "Los Polvorines",
    "latitude": -34.533760, 
    "longitude": -58.692730,
    "descripcion_del_lugar" : " This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', comes from a line in section 1.10.32.",
    "descripcion_de_actividades" : "arte abstracto",
  }
];


var usuarios = [
  {
    "usuario" : 'Daniel',
    "contraseña": '1234',
    "rol": 'Representante'
  },
  {
    "usuario" : 'Yair',
    "contraseña": '1234',
    "rol": 'Representante'
  },
 
  {
    "usuario" : 'Matias',
    "contraseña": '1234',
    "rol": 'Artista'
  },
];

////////////////////////////FUNCIONES /////////////////////////////////////

function ingreso() {  // dependiendo si es representante o artista iniciara sesion diferente
  var u= document.getElementById('u').value;
  var p=document.getElementById('p').value;


      for (let index = 0; index < usuarios.length; index++) {
           var UsuarioRegistrado = usuarios[index];
        if(UsuarioRegistrado.usuario==u && UsuarioRegistrado.contraseña===p && UsuarioRegistrado.rol=='Artista' ){
            window.location.href="ingresoComoArtista.html";
 
        } 
        if(UsuarioRegistrado.usuario==u && UsuarioRegistrado.contraseña===p && UsuarioRegistrado.rol=='Representante'){
          window.location.href="ingresoComoRepresentante.html";
       } 
  
      }

}

////////////////// FUNCION DE BUSQUEDAS/////////////////////

  var inicio = [-34.5221554, -58.7000067];
  //ubicacion inicial
  var mapa = L.map('mapaid').setView(inicio, 15);
  //mapa en la pagina
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(mapa);


var items = [];
var features = [];
//////////////mostrar todos los talleres/////////////////

$("#ver_todos_talleres").click(function() {
  limpiar();
  div_inf = $('.info').empty();
 $(talleres).filter(function (i,n){
 
     items.push(n); //agrego todos los talleres en un arreglo
   
});


 
for (var i = 0; i < items.length; i++) {

   var item = items[i];
   var longitude = item.longitude;
   var latitude = item.latitude;
   var descripcion_del_lugar = item.descripcion_del_lugar;
   var descripcion_de_actividades = item.descripcion_de_actividades;
   //agregar informacion en los puntos
   var desc_point = '<b> Taller: </b>'+ item.nombre_taller +'<br></br>' + '<b> Direccion: </b>' + item.direccion  ;
    //agregar en el mapa
   var marker = L.marker([item.latitude,item.longitude]).addTo(mapa);
  // muestra en el mapa
   marker.bindPopup(desc_point); 
   features.push(marker);    
   div_inf.append( "<div class= 'texto'>"+ "<h3> Nombre del taller </h3>" + item.nombre_taller + "<h3> Localidad </h3>"+ item.localidad + 
  "<h3> Descripcion de las Actividades </h3>"+ item.descripcion_de_actividades+"<br></br>"+"</div>"
    );
 }    


});
///////////////// mostrar solo el buscado//////////
$("#btn_busqueda").click(function() {
  limpiar();
  valor = $('#barra_busqueda').val().toLowerCase();
  $(talleres).filter(function (i,n){
    if(n.nombre_taller.toLowerCase()==valor||n.descripcion_de_actividades.toLowerCase()==valor||n.localidad.toLowerCase()==valor){
      items.push(n);
    }
});


  
for (var i = 0; i < items.length; i++) {
    div_inf = $('.info').empty();//vaciar
    var item = items[i];
    var longitude = item.longitude;
    var latitude = item.latitude;
    var descripcion_del_lugar = item.descripcion_del_lugar;
    var descripcion_de_actividades = item.descripcion_de_actividades;
    var desc_point = '<b> Taller: </b>'+ item.nombre_taller +'<br></br>' + '<b> Direccion: </b>' + item.direccion  ;
     //agregar en el mapa
    var marker = L.marker([item.latitude,item.longitude]).addTo(mapa);
   // muestra en el mapa
    mapa.setView([item.latitude,item.longitude], 15);
    marker.bindPopup(desc_point); 
    features.push(marker);    
    div_inf.append( "<div class= 'texto'>"+ "<h3> Nombre del taller </h3>" + item.nombre_taller + "<h3> Localidad </h3>"+ item.localidad + 
    "<h3> Descripción del lugar </h3>" +item.descripcion_del_lugar + "<h3> Descripcion de las Actividades </h3>"+ item.descripcion_de_actividades+"</div>"
     );    
  }    


});
       //mostrar puntos en el mapa//
function mostrar_popup(e) {
  L.featureGroup(features).openPopup();
}
mapa.on('click', mostrar_popup);

////////limpiar//////
function limpiar(){ // borro el mapa y lo vuelvo a inicializar con las variables
  mapa.remove();
  items = [];
  features = [];
  div_inf = $('#info').empty();
  mapa = L.map('mapaid').setView(inicio, 15);
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(mapa);

}
